﻿// MJB UQAC/NAD


#include "Ashtray.h"

#include "MJBProd/Helpers/UtilityHelpers.h"


// Sets default values
AAshtray::AAshtray()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AAshtray::InsertCigarette()
{
	if(CigQuantity >= sizeof CigMeshes) return;

	UStaticMeshComponent *CigMesh = CigMeshes[CigQuantity];
	
	if(CigMesh == nullptr) return;

	CigMesh->SetVisibility(true);
	CigQuantity++;
}

// Called when the game starts or when spawned
void AAshtray::BeginPlay()
{
	GetComponents<UStaticMeshComponent>(CigMeshes);
	CigMeshes.RemoveAt(0); //MainMesh
	
	Super::BeginPlay();
}

// Called every frame
void AAshtray::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

