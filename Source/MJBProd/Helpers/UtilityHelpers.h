// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class MJBPROD_API UtilityHelpers
{
	
public:
	UtilityHelpers();
	~UtilityHelpers();

	static void Log(FString Msg, bool AtScreen = false, UObject* object = nullptr);
	static void LogFloat(float Msg, bool AtScreen = false, UObject* object = nullptr);
	static void LogBool(bool Msg, bool AtScreen = false, UObject* object = nullptr);
	static void LogWarning(FString Msg, bool AtScreen = false, UObject* object = nullptr);
	static void LogError(FString Msg, bool AtScreen = false, UObject* object = nullptr);

	private:
	static void PrintToScreen(FString Msg, bool AtScreen = true);
};
