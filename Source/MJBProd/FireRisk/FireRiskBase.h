﻿// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FireRiskBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDangerUpdate);
UCLASS()
class MJBPROD_API AFireRiskBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFireRiskBase();

private:
	// Function
	void EnableFirePropagation() const;
protected:
	// Function
	virtual void BeginPlay() override;

	// Attributes

	UPROPERTY()
	int BurnForestRadius;
	
public:
	// Function
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
	virtual void Activate();
	
	UFUNCTION()
	virtual void Deactivate();

	UFUNCTION(BlueprintCallable)
	virtual void UnpauseTimerFirePropagation();

	UFUNCTION(BlueprintCallable)
	virtual void PauseTimerFirePropagation();

	// Attributes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=FireRisk)
	bool IsDangerous;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=FireRisk)
	float TimeBeforePropagate;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=FireRisk)
	float TimeBeforeFirstPropagation;

	UPROPERTY(BlueprintReadWrite,EditAnywhere , Category="Task")
	int TaskNumber;

	UPROPERTY()
	FTimerHandle FirePropagationTimer;

	UPROPERTY()
	bool IsSafe;

	// Events
	UPROPERTY(BlueprintAssignable, Category = "FireRisk Event")
	FDangerUpdate OnDangerUpdate;
};
