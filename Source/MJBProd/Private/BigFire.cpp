// MJB UQAC/NAD


#include "BigFire.h"

#include "ChaosInterfaceWrapperCore.h"
#include "Chaos/CollisionResolutionUtil.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MJBProd/MJBProdCharacter.h"

class AMJBProdCharacter;
// Sets default values
ABigFire::ABigFire()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABigFire::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABigFire::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

