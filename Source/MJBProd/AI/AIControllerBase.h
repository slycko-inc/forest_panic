// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor.h"
#include "AICharacterBase.h"
#include "AIController.h"
#include "MJBProd/GameModes/MJBProdGameMode.h"
#include "AIControllerBase.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FLifeUpdate);
UCLASS()
class MJBPROD_API AAIControllerBase : public AAIController 
{
	GENERATED_BODY()
	FTimerHandle PatrolTimerHandle;
	FTimerHandle BurnTimerHandle;
protected:
	virtual void BeginPlay() override;
	void Patrol();
	void DoPatrol();

	UFUNCTION(BlueprintCallable)
	static void OnPerceptionUpdated(AActor *Actor, AAIControllerBase *Controller);
	  

private:
	void Die();
	
public:
	AAIControllerBase();
	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	static FVector FindPatrolPoint(AAICharacterBase *Charact, float Radius);
	
	UFUNCTION(BlueprintCallable)
	static void BurnForest(AAIControllerBase *Control);

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FLifeUpdate OnLifeUpdate;
	
	// AI base attributes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiCaracteristics)
	 bool IsMoving;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiCaracteristics)
	bool IsAggressive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	bool IsEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	float CurrentLife;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	float MaxLife;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	bool bIsDead;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	bool bIsShieldActivate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	float TimeBeforePatrol = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	float TimeBetweenPatrol = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=AiComponnent)
	float TimeBetweenForestBurn = 5.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Attack")
	int NbFireSetByAttack;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Attack")
	int BurnForestAttackRadius;
	
	UFUNCTION(BlueprintCallable)
	void Damage(float Damage);

	void RemoveShield();

	AMJBProdGameMode* LevelOneGameMode;
};
