// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"
#include "FireProjectile.generated.h"

/**
 * 
 */
UCLASS()
class MJBPROD_API AFireProjectile : public AProjectileBase
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
public:
	AFireProjectile();
	virtual void Tick(float DeltaTime) override;
	virtual void FireInDirection(const FVector& ShootDirection) override;
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
		FVector NormalImpulse, const FHitResult& Hit) override;
};
