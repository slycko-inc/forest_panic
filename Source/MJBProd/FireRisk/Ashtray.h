﻿// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ashtray.generated.h"

UCLASS()
class MJBPROD_API AAshtray : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<UStaticMeshComponent*> CigMeshes;
	
	int CigQuantity;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this actor's properties
	AAshtray();

	void InsertCigarette();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Attributes
	UPROPERTY(BlueprintReadWrite,EditAnywhere , Category="Task")
	int TaskNumber;
};
