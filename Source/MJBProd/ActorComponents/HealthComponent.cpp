﻿// MJB UQAC/NAD


#include "HealthComponent.h"
#include "GameFramework/Actor.h"
#include <string>
#include "MJBProd/MJBProdCharacter.h"
#include "MJBProd/Helpers/UtilityHelpers.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	MaxHealth = 100;

	CurrentHealth = MaxHealth;
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* Owner = GetOwner();

	if(Owner)
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle,this,&UHealthComponent::Regen,3.0f,true,0.5f);
}

void UHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <= 0)
	{
		return;
	}
	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.0f, MaxHealth);

	if(CurrentHealth <= 0.0f)
	{
		AMJBProdCharacter* Character = Cast<AMJBProdCharacter>(DamagedActor);
		Character->OnDeath.Broadcast();
	}
}

void UHealthComponent::Regen()
{
	if(CurrentHealth < MaxHealth)
	{
		if(CurrentHealth+2 <= MaxHealth)
		{
			CurrentHealth = CurrentHealth+2;
		}
	}
}

