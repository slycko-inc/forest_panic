// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "MJBProd/FireRisk/Ashtray.h"
#include "MJBProd/GameModes/MJBProdGameModeBase.h"
#include "MJBProd/FireRisk/Cigarette.h"
#include "AreaTwoGameMode.generated.h"


/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FZoneTwoDone);
UCLASS()
class MJBPROD_API AAreaTwoGameMode : public AMJBProdGameModeBase
{
	GENERATED_BODY()
	AAreaTwoGameMode();
	void GetTask(int TaskNumber, ACigarette* &CigaretteRef, AAshtray* &AshtrayRef);
	static void ActivateTask(ACigarette* Cigarette, AAshtray* Ashtray);
protected:
	virtual void BeginPlay() override;
public:

	// Function
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginTask() override;
	virtual void EndTask() override;

	// Attributes
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ACigarette*> Cigarettes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AAshtray*> Ashtrays;
	
	UPROPERTY()
	TArray<AActor*> TCigarettes;
	
	UPROPERTY()
	TArray<AActor*> TAshtrays;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsAreaTwoCompleted;
	
	// Events
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Events")
	FZoneTwoDone ZoneTwoDone;
};
