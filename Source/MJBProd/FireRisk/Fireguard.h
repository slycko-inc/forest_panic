// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Fireguard.generated.h"

UCLASS()
class MJBPROD_API AFireguard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFireguard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Attributes
	UPROPERTY(BlueprintReadWrite,EditAnywhere , Category="Task")
	int TaskNumber;

};
