// Fill out your copyright notice in the Description page of Project Settings.


#include "Water_Projectile.h"

#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MJBProd/AI/AIControllerBase.h"
#include "MJBProd/GameModes/MJBProdGameModeBase.h"
#include "MJBProd/GameModes/TutorialGameMode.h"
#include "MJBProd/Helpers/UtilityHelpers.h"
#include "MJBProd/SceneComponents/SC_Flammable.h"

// Sets default values
AWater_Projectile::AWater_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->Damage = 7.0f;

	if(!RootComponent)
	{
		RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSceneComponent"));
	}
	if(!CollisionComponent)
	{
		CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
		CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Projectile"));
		CollisionComponent->OnComponentHit.AddDynamic(this, &AWater_Projectile::OnHit);
		CollisionComponent->InitSphereRadius(10.0f);
		RootComponent = CollisionComponent;
	}

	if(!ProjectileMovementComponent)
	{
		ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
		ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
		ProjectileMovementComponent->InitialSpeed = 2000.0f;
		ProjectileMovementComponent->MaxSpeed = 2000.0f;
		ProjectileMovementComponent->bRotationFollowsVelocity = true;
		ProjectileMovementComponent->bShouldBounce = true;
		ProjectileMovementComponent->Bounciness = 0.3f;
		ProjectileMovementComponent->ProjectileGravityScale = 0.75f;
	}

	if(!ProjectileMeshComponent)
	{
		ProjectileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMeshComponent"));
		static ConstructorHelpers::FObjectFinder<UStaticMesh>Mesh(TEXT("'/Game/Meshes/SM_Projectile.SM_Projectile'"));
		if(Mesh.Succeeded())
		{
			ProjectileMeshComponent->SetStaticMesh(Mesh.Object);
		}
	}
	
	ProjectileMeshComponent->SetRelativeScale3D(FVector(1.5f, 1.5f, 1.5f));
	ProjectileMeshComponent->SetupAttachment(RootComponent);

	// Init du son d'impact du projectile
	if(!ImpactAudioCue)
	{
		static ConstructorHelpers::FObjectFinder<USoundCue> impactCue(TEXT("'/Game/Sound/water_projectile_impact/WaterProjectileImpactSoundCue.WaterProjectileImpactSoundCue'"));
		ImpactAudioCue = impactCue.Object;
	}
}

void AWater_Projectile::CallDestroy()
{
	if(ImpactAudioCue)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactAudioCue, this->GetActorLocation());
	}

	this->Destroy();
}

// Called when the game starts or when spawned
void AWater_Projectile::BeginPlay()
{
	Super::BeginPlay();
	CurrentGameMode = static_cast<AMJBProdGameModeBase*>(GetWorld()->GetAuthGameMode());
}

// Called every frame
void AWater_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWater_Projectile::FireInDirection(const FVector& ShootDirection)
{
	Super::FireInDirection(ShootDirection);
	// ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}

void AWater_Projectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	//TODO Générer des événements seulement pour les objects nécessaires
	
	AAIControllerBase* Boss = Hit.GetActor()->GetInstigatorController<AAIControllerBase>();
	if(Boss != nullptr)
	{
		UtilityHelpers::Log("Hit: " + Hit.GetActor()->GetHumanReadableName(),false, this);
		Boss->Damage(this->Damage);
		this->CallDestroy();
		return;
	}

	USC_Flammable* SC_FlammableComponent = Hit.GetActor()->FindComponentByClass<USC_Flammable>();
	if(SC_FlammableComponent != nullptr)
	{
		if(SC_FlammableComponent->GetIsOnFire())
		{
			if(CurrentGameMode->IsInTutorial)
			{
				CurrentGameMode->TutorialIsTreeNotOnFire = true;
				SC_FlammableComponent->SetIsOnFire(false);
				SC_FlammableComponent->OnFireStopped.Broadcast();
				if(CurrentGameMode->TutorialIsFireguardPutOnFire)
					CurrentGameMode->OnTutorialLastFireStopped.Broadcast();
			}
			else
			{
				if(CurrentGameMode->TreeInFireCount != 0 && (CurrentGameMode->TreeInFireCount < CurrentGameMode->MaxTreeInFireCount))
				{
					CurrentGameMode->RemoveTreeFireCount(1);
				}
			}
		}
		
		UtilityHelpers::Log("Hit: " + Hit.GetActor()->GetHumanReadableName(),false, this);
		if(!CurrentGameMode->IsInTutorial)
		{
			SC_FlammableComponent->SetIsOnFire(false);
		}
		
		this->CallDestroy();
		return;
	}

	UStaticMeshComponent *Mesh = OtherActor->FindComponentByClass<UStaticMeshComponent>();
	if(Mesh != nullptr && Mesh->Mobility == EComponentMobility::Movable)
	{
		const ACharacter *Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		FVector Direction = OtherActor->GetActorLocation() - Player->GetActorLocation();
		Direction.Normalize();
		
		UtilityHelpers::Log(Direction.ToString(), true, this);
		Mesh->AddImpulse(Direction * ImpactForce);
	}
	
	this->CallDestroy();

}
