// Fill out your copyright notice in the Description page of Project Settings.


#include "AIControllerBase.h"

#include "AICharacterBase.h"
#include "DrawDebugHelpers.h"
#include "MJBProd/MJBProdCharacter.h"
#include "NavigationSystem.h"
#include "AI/NavigationSystemBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MJBProd/Helpers/UtilityHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MJBProd/SceneComponents/SC_Flammable.h"
#include "Niagara/Public/NiagaraComponent.h"

void AAIControllerBase::BeginPlay()
{
	Super::BeginPlay();
	Patrol();

	this->bIsDead = false;
	this->bIsShieldActivate = true;
	this->NbFireSetByAttack = -1;
	this->BurnForestAttackRadius = 500;
	this->MaxLife = 100.0f;
	this->CurrentLife = this->MaxLife;
	LevelOneGameMode = static_cast<AMJBProdGameMode*>(GetWorld()->GetAuthGameMode());
}

void AAIControllerBase::Patrol()
{
}

void AAIControllerBase::DoPatrol()
{
	FNavLocation ResultLocation;
	FVector SelfLocation = GetPawn()->GetTransform().GetLocation();
	auto NavSystem = UNavigationSystemV1::CreateNavigationSystem(GEngine->GetWorldContexts()[0].World());
	auto NavData = Cast<ANavigationData>(
		FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld())->GetMainNavData()
	);

	// Get random point, changed radius if needed
	NavSystem->GetRandomReachablePointInRadius(SelfLocation, 10000.0f, ResultLocation, NavData);

	MoveTo(FAIMoveRequest(ResultLocation.Location));
}

void AAIControllerBase::BurnForest(AAIControllerBase *Control)
{
	if(Control->bIsDead)
		return;
	// On entre en collision avec PhysicsBody et Flammable(GameTraceChannel2).
	// Voir DefaultGame.ini pour definition.
	auto ObjectTypes = TArray<TEnumAsByte<EObjectTypeQuery>>({
		UEngineTypes::ConvertToObjectType(ECC_PhysicsBody),
		UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel2)
	});

	TArray<FHitResult> Hits = TArray<FHitResult>();
	const TArray<AActor*> IgnoreList = TArray<AActor*>();

	UNiagaraComponent *Particles = Control->GetPawn()->FindComponentByClass<UNiagaraComponent>();
	Particles->Activate(true);

	const FVector SelfLocation = Control->GetPawn()->GetTransform().GetLocation();

	UKismetSystemLibrary::SphereTraceMultiForObjects(
		Control->GetWorld(),
		SelfLocation,
		SelfLocation,
		Control->BurnForestAttackRadius,
		ObjectTypes,
		true,
		IgnoreList,
		EDrawDebugTrace::None,
		Hits,
		true
	);

	int NbObjectSetOnFire = 0;
	for (FHitResult hit : Hits)
	{
		if (!Control->NbFireSetByAttack == 0)
		{
			USC_Flammable* SC_FlammableComponent = hit.GetActor()->FindComponentByClass<USC_Flammable>();
			
				if (SC_FlammableComponent != nullptr)
				{
					UtilityHelpers::Log(hit.GetActor()->GetHumanReadableName(), false, Control);

					SC_FlammableComponent->SetIsOnFire(true);
					break;
				}
		}

		AMJBProdCharacter* Player = Cast<AMJBProdCharacter>(hit.GetActor());

		if (Player != nullptr)
		{
			FVector Force = Player->GetTransform().GetLocation() - SelfLocation;
			Force.Normalize(1);
			Force = Force * FVector(1500, 1500, 500);

			UGameplayStatics::ApplyDamage(hit.GetActor(),18,nullptr, nullptr, nullptr);
			Player->ReceiveKnockback(Force);
		}
	}
}

void AAIControllerBase::OnPerceptionUpdated(AActor *Actor, AAIControllerBase *Controller)
{
}

void AAIControllerBase::Die()
{
	UtilityHelpers::Log("IsDead", false, this);
	this->bIsDead = true;
	GetCharacter()->GetCharacterMovement()->DisableMovement();
	
	LevelOneGameMode->OnGameWin.Broadcast();
	
}

AAIControllerBase::AAIControllerBase()
{
}

void AAIControllerBase::Tick(float DeltaTime)
{
	if(LevelOneGameMode->FireGolemCanBeAttacked && bIsShieldActivate)
		RemoveShield();
	
	Super::Tick(DeltaTime);
}

FVector AAIControllerBase::FindPatrolPoint(AAICharacterBase *Charact, float Radius)
{
	FNavLocation ResultLocation;
	FVector SelfLocation = Charact->GetTransform().GetLocation();
	auto NavSystem = UNavigationSystemV1::GetNavigationSystem(GEngine->GetWorldContexts()[0].World());
	auto NavData = Cast<ANavigationData>(FNavigationSystem::GetCurrent<UNavigationSystemV1>(Charact->GetWorld())->GetMainNavData());

	// Get random point, changed radius if needed
	NavSystem->GetRandomReachablePointInRadius(SelfLocation, Radius, ResultLocation, NavData);

	return ResultLocation.Location;
}

void AAIControllerBase::Damage(float Damage)
{
	if (!this->bIsShieldActivate)
	{
		this->CurrentLife -= Damage;
		this->OnLifeUpdate.Broadcast();	
	}

	UtilityHelpers::Log("Life: " + UKismetStringLibrary::Conv_FloatToString(this->CurrentLife), false, this);

	if (CurrentLife <= 0)
		this->Die();
}

void AAIControllerBase::RemoveShield()
{
	bIsShieldActivate = false;
	
	AAICharacterBase *BaseCharacter = Cast<AAICharacterBase>(GetPawn());
	USkeletalMeshComponent *Mesh = BaseCharacter->FindComponentByClass<USkeletalMeshComponent>();
	if(Mesh != nullptr && BaseCharacter != nullptr)
	{
		if(BaseCharacter->VulnerableMaterial != nullptr){
			Mesh->SetMaterial(0, BaseCharacter->VulnerableMaterial);
		}
	}
}
