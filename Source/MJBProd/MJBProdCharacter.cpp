// Copyright Epic Games, Inc. All Rights Reserved.

#include "MJBProdCharacter.h"

#include "FireRisk/Campfire.h"
#include "DrawDebugHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "FireRisk/Ashtray.h"
#include "FireRisk/Cigarette.h"
#include "FireRisk/Fireguard.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Helpers/UtilityHelpers.h"
#include "Interface/Interact.h"
#include "Kismet/KismetSystemLibrary.h"

//////////////////////////////////////////////////////////////////////////
// AMJBProdCharacter

void AMJBProdCharacter::Dead()
{
	IsDead = true;

	OnDeathAnimStart.Broadcast();
	
	//Timer pour attendre l'anim
	//GameOver
}

AMJBProdCharacter::AMJBProdCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	IsDead = false;
	isZoomIn = false;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	Gun_Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun_Mesh"));
	Gun_Mesh->bCastDynamicShadow = true;
	Gun_Mesh->CastShadow = true;
	Gun_Mesh->SetupAttachment(GetMesh(), TEXT("r_wrist_jntSocket"));

	ProjectileSpawnerLocation = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSpawnerLocation"));
	ProjectileSpawnerLocation->SetupAttachment(Gun_Mesh);
	
	CameraBoom->AddRelativeLocation(FVector(0, 0, CameraOffset));

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMJBProdCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AMJBProdCharacter::Interact);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMJBProdCharacter::Fire);

	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &AMJBProdCharacter::ZoomIn);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &AMJBProdCharacter::ZoomOut);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMJBProdCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMJBProdCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMJBProdCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMJBProdCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AMJBProdCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AMJBProdCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AMJBProdCharacter::OnResetVR);

	PlayerInputComponent->BindAction("Debug", IE_Pressed, this, &AMJBProdCharacter::ToggleDebug);
}

void AMJBProdCharacter::Fire()
{
	if (ProjectileClass)
	{
		FRotator SpawnRotation = GetActorRotation();
		const FVector SpawnLocation = (ProjectileSpawnerLocation != nullptr)
			                              ? ProjectileSpawnerLocation->GetComponentLocation()
			                              : GetActorLocation();

		SpawnRotation.Pitch += 5.0f;

		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = GetInstigator();

			AWater_Projectile* Projectile = World->SpawnActor<AWater_Projectile>(
				ProjectileClass, SpawnLocation, SpawnRotation, SpawnParams);
			if (Projectile)
			{
				FireProjectile = true;
				FVector LaunchDirection = SpawnRotation.Vector();
				Projectile->FireInDirection(LaunchDirection);
			}
		}
	}
}

void AMJBProdCharacter::ZoomIn()
{
	UtilityHelpers::Log(GetActorRotation().ToString(), true, this);
	if (auto thirdPersonCamera = GetCameraBoom())
	{
		GetMesh()->SetVisibility(false);
		thirdPersonCamera->AttachToComponent(GetMesh(),FAttachmentTransformRules::SnapToTargetNotIncludingScale,TEXT("rootSocket"));
		thirdPersonCamera->TargetArmLength = 0.0f;
		bUseControllerRotationYaw = true;
		bUseControllerRotationPitch = true;
		bUseControllerRotationRoll = true; 

		if (auto characterMovement = GetCharacterMovement())
		{
			characterMovement->MaxWalkSpeed = 300.0f;
			characterMovement->bOrientRotationToMovement = false;
			characterMovement->bAllowPhysicsRotationDuringAnimRootMotion = false;
			characterMovement->bUseControllerDesiredRotation = false;
		}

		isZoomIn = true;
	}
}

void AMJBProdCharacter::ZoomOut()
{
	if (auto thirdPersonCamera = GetCameraBoom())
	{
		const FRotator Rot = FRotator(0, GetActorRotation().Yaw, 0);
		SetActorRotation(Rot);
		GetMesh()->SetVisibility(true);
		thirdPersonCamera->AttachToComponent(GetCapsuleComponent(),FAttachmentTransformRules::SnapToTargetIncludingScale,"");
		thirdPersonCamera->TargetArmLength = 300.0f;
		bUseControllerRotationYaw = false;
		bUseControllerRotationPitch = false;
		bUseControllerRotationRoll = false;

		if (auto characterMovement = GetCharacterMovement())
		{
			characterMovement->MaxWalkSpeed = 600.0f;
			characterMovement->bOrientRotationToMovement = true;
			characterMovement->bAllowPhysicsRotationDuringAnimRootMotion = true;
			characterMovement->bUseControllerDesiredRotation = true;
		}

		CameraBoom->SetRelativeLocation(FVector(0, 0, CameraOffset));

		isZoomIn = false;
	}
}


void AMJBProdCharacter::OnResetVR()
{
	// If MJBProd is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in MJBProd.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMJBProdCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AMJBProdCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AMJBProdCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMJBProdCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMJBProdCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		CheckInteract();
	}
}

void AMJBProdCharacter::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AMJBProdCharacter::ReceiveKnockback(FVector Force, int Damage)
{
	DisableInput(Cast<APlayerController>(GetController()));
	GetCharacterMovement()->Launch(Force);

	GetWorldTimerManager().SetTimer(
		StunTimerHandle,
		this,
		&AMJBProdCharacter::DisableStun,
		1,
		false,
		0.5f
	);
}

void AMJBProdCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentGameMode = static_cast<AMJBProdGameModeBase*>(GetWorld()->GetAuthGameMode());
	OnDeath.AddDynamic(this, &AMJBProdCharacter::Dead);
}

void AMJBProdCharacter::DisableStun()
{
	EnableInput(Cast<APlayerController>(GetController()));
}

void AMJBProdCharacter::CheckInteract()
{
	FHitResult Hit;
	FCollisionQueryParams CollisionParams;

	GetWorld()->LineTraceSingleByChannel(
		Hit,
		GetActorLocation(),
		GetActorLocation() + GetActorForwardVector() * 50,
		ECC_GameTraceChannel3,
		CollisionParams
	);

	if (Hit.Actor != nullptr)
	{
		ACampfire *Campfire = Cast<ACampfire>(Hit.Actor);
		if(Campfire != nullptr)
		{
			if(Campfire->IsDangerous && HasFireGuard)
			{
				//UtilityHelpers::Log(TEXT("Campfire Can interract"),true, this);
				OnCanInteract.Broadcast();
			}
			else
			{
				//UtilityHelpers::Log(TEXT("Campfire Cannot interract"),true, this);
				OnCannotInteract.Broadcast();
			}
			return;
		}

		AFireguard *Fireguard = Cast<AFireguard>(Hit.Actor);
		if(Fireguard != nullptr)
		{
			if(HasFireGuard)
			{
				//UtilityHelpers::Log(TEXT("Fireguard cannot"),true, this);
				OnCannotInteract.Broadcast();
			}
			else
			{
				//UtilityHelpers::Log(TEXT("Fireguard can interact"),true, this);
				OnCanInteract.Broadcast();
			}
			return;
		}

		ACigarette *Cigarette = Cast<ACigarette>(Hit.Actor);
		if(Cigarette != nullptr)
		{
			if(HasCigarette)
			{
				//UtilityHelpers::Log(TEXT("Fireguard cannot"),true, this);
				OnCannotInteract.Broadcast();
			}
			else
			{
				//UtilityHelpers::Log(TEXT("Fireguard can interact"),true, this);
				OnCanInteract.Broadcast();
			}
			return;
		}

		AAshtray *Ashtray = Cast<AAshtray>(Hit.Actor);
		if(Ashtray != nullptr)
		{
			if(HasCigarette)
				OnCanInteract.Broadcast();
			else
				OnCannotInteract.Broadcast();
		}
		
		// Si autre obj, comme le npc
		if (Hit.GetActor()->GetClass()->ImplementsInterface(UInteract::StaticClass()))
		{
			//UtilityHelpers::Log(TEXT("npc can interact"),true, this);
			OnCanInteract.Broadcast();
		}
	}
	else
	{
		OnCannotInteract.Broadcast();
	}
}

void AMJBProdCharacter::Interact()
{
	FHitResult Hit;
	FCollisionQueryParams CollisionParams;

	GetWorld()->LineTraceSingleByChannel(
		Hit,
		GetActorLocation(),
		GetActorLocation() + GetActorForwardVector() * 50,
		ECC_GameTraceChannel3,
		CollisionParams
	);

	if (Hit.Actor != nullptr)
	{
		if (Hit.GetActor()->GetClass()->ImplementsInterface(UInteract::StaticClass()))
		{
			UtilityHelpers::Log("Interact with: " + Hit.GetActor()->GetClass()->GetName(), false, this);
			IInteract* interactableActor = Cast<IInteract>(Hit.GetActor());
			interactableActor->Execute_Interact(Hit.GetActor());
			return;
		}

		auto components = GetComponents();
		for (auto component : components)
		{
			if (component->GetFName() == "FireGuard")
			{
				FireGuardMesh = dynamic_cast<UStaticMeshComponent*>(component);
			}

			if (component->GetFName() == "Cigarette")
			{
				CigaretteMesh = dynamic_cast<UStaticMeshComponent*>(component);
			}
		}

		ACampfire* Campfire = Cast<ACampfire>(Hit.Actor);
		ACigarette* Cigarette = Cast<ACigarette>(Hit.Actor);
		AAshtray *Ashtray = Cast<AAshtray>(Hit.Actor);
		
		if (HasFireGuard)
		{
			if (Campfire != nullptr)
			{
				if(!Campfire->IsDangerous) return;
				
				Campfire->Deactivate();
				HasFireGuard = false;
				OnCannotInteract.Broadcast();
				
				OnDropAnim.Broadcast(EPickable::E_FireGuard);
				FireGuardMesh->SetVisibility(false);

				if(!CurrentGameMode->IsInTutorial)
				{
					CurrentGameMode->IsCurrentTaskCompleted = true;
					CurrentGameMode->OnEndTask.Broadcast();
				}
				else
				{
					CurrentGameMode->TutorialIsFireguardPutOnFire = true;

					if(CurrentGameMode->TutorialIsTreeNotOnFire)
						CurrentGameMode->OnTutorialLastFireStopped.Broadcast();
				}
			}
		}
		else
		{
			if (FireGuardMesh != nullptr && Campfire == nullptr && Cigarette == nullptr && Ashtray == nullptr)
			{
				FireGuardMesh->SetVisibility(true);
				HasFireGuard = true;
				
				OnPickupAnim.Broadcast(EPickable::E_FireGuard);
				OnCannotInteract.Broadcast();
				if (!Hit.GetActor()->IsPendingKill())
					Hit.Actor->Destroy();
			}
		}
		
		if (HasCigarette)
		{
			if(Ashtray != nullptr && CigaretteMesh != nullptr)
			{
				Ashtray->InsertCigarette();
				OnDropAnim.Broadcast(EPickable::E_Cigarette);

				OnCannotInteract.Broadcast();
				HasCigarette = false;
				CigaretteMesh->SetVisibility(false);
				CurrentGameMode->OnDropCigarette.Broadcast();

				CurrentGameMode->IsCurrentTaskCompleted = true;
				CurrentGameMode->OnEndTask.Broadcast();
			}
		}
		else
		{
			if (CigaretteMesh != nullptr && Cigarette != nullptr)
			{
				CigaretteMesh->SetVisibility(true);
				HasCigarette = true;
				Cigarette->Deactivate();
				
				CurrentGameMode->OnPickupCigarette.Broadcast();
				OnPickupAnim.Broadcast(EPickable::E_Cigarette);
				OnCannotInteract.Broadcast();
				if (!Hit.GetActor()->IsPendingKill())
					Hit.Actor->Destroy();
			}
		}
	}
}

void AMJBProdCharacter::ToggleDebug()
{
	UtilityHelpers::Log(TEXT("Toggle Debug"), false, this);
	FString Command = "Stat FPS";

	GetNetOwningPlayer()->ConsoleCommand(*Command);
}
