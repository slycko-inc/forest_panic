# Forest Panic
A UE 5 game made during courses of Bachelor's degree in video game design at University of Quebec at Chicoutimi, at the autumn 2022 trimestral. 

## Summary 
This is a serious game created as part of a university course. The aim of the game is to teach young people how to prevent forest fires. Made with the collaboration of SOPFEU.

## Core feature

- Interact with environement to prevent wildfire
- Complete loop
- Polish (we have worked during 6 month for this project)
- [Meaningful Play 2022](https://meaningfulplay.msu.edu/index.php) finalist

## Credits

Programmers

Marc-Antoine Jean : Lead programmer

Jérémy Laforge-Riverin : Generalist programmer

Philippe Milot : Generalist programmer

Jean-Benoît Tremblay : Generalist programmer



Artists

Jonathan Argueta Domenis : VFX and environment 

Florence Beaudoin : Level art and environment

Guillaume Levasseur : Animator

Mai-Kim Morin : Level art and environment

Océane Nadeau : Level art, environment and light

Daphnée Péloquin : Level art and environment

Daphné Perron : Character artist

Véronic Sabourin : Environment and UI

Thomas Trang : Rig artist


Sounds

Alexandre Hervieux : Lead audio

Rodrigo Murillo : Writer

Noah Wright : Sound designer