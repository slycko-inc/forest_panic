// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MJBProd : ModuleRules
{
	public MJBProd(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "Niagara" });
	}
}
