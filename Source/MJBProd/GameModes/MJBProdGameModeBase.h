// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MJBProdGameModeBase.generated.h"

class USoundCue;
/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameWin);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTreeOnFireCountUpdate);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBeginTask);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEndTask);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTaskCompletedWithTreeInFire);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTaskCompletedWithoutTreeInFire);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStartEndTaskDialog);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTutorialLastFireStopped);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTutorialLastFireguard);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOverFire);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPickupCigarette);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDropCigarette);

UCLASS()
class MJBPROD_API AMJBProdGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	AMJBProdGameModeBase();
public:
	// Function
	UFUNCTION()
	virtual void BeginTask();
	
	UFUNCTION()
	virtual void EndTask();
	
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	void AddTreeFireCount(int NbToAdd);

	UFUNCTION(BlueprintCallable)
	void RemoveTreeFireCount(int NbToRemove);

	// Attributes
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool IsDialogWritingCompleted;
    	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int TreeInFireCount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxTreeInFireCount = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool GameOver;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsInTutorial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool GameWin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool ZeroTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentTaskNumber = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsCurrentTaskCompleted;

	// Tuto base attributes
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool TutorialIsFireguardPutOnFire;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool TutorialIsTreeNotOnFire;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
	USoundCue* TaskCompletedAudioCue;

	// Events
	UPROPERTY(BlueprintAssignable,BlueprintCallable, Category = "Events")
	FOnGameWin OnGameWin;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Events")
	FBeginTask OnBeginTask;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FTutorialLastFireStopped OnTutorialLastFireStopped;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Events")
	FPickupCigarette OnPickupCigarette;
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Events")
	FDropCigarette OnDropCigarette;

	// Define in ThirdPersonCaracter
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FTreeOnFireCountUpdate OnTreeFireCountUpdate;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FEndTask OnEndTask;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FTaskCompletedWithTreeInFire OnTaskCompletedWithTreeInFire;

	//event game over
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FGameOverFire GameOverFire;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FTaskCompletedWithoutTreeInFire OnTaskCompletedWithOutTreeInFire;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Events")
	FStartEndTaskDialog OnStartEndTaskDialog;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Events")
	FTutorialLastFireguard OnTutorialLastFireguard;
};
