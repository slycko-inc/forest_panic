// MJB UQAC/NAD

#pragma once

#include <list>

#include "CoreMinimal.h"
#include "Actor.h"
#include "MJBProdGameModeBase.h"
#include "GameFramework/GameModeBase.h"
#include "MJBProd/FireRisk/Campfire.h"
#include "MJBProd/FireRisk/Fireguard.h"
#include "AreaOneGameMode.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FZoneOneDone);
UCLASS()
class MJBPROD_API AAreaOneGameMode : public AMJBProdGameModeBase
{
	GENERATED_BODY()
	AAreaOneGameMode();
	void GetTask(int TaskNumber, ACampfire* &CampfireRef, AFireguard* &FireguardRef);
	static void ActivateTask(ACampfire* Campfire, AFireguard* Fireguard);
protected:
	virtual void BeginPlay() override;
public:
	// Function
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void BeginTask() override;
	
	virtual void EndTask() override;
	
	// Attributes
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ACampfire*> Campfires;

	UPROPERTY()
	TArray<AActor*> TCampfires;

	UPROPERTY()
	TArray<AActor*> TFireGuards;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AFireguard*> Fireguards;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsAreaOneCompleted;

	// Events
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Events")
	FZoneOneDone ZoneOneDone;
};