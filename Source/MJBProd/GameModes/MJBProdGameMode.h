// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MJBProdGameModeBase.h"
#include "GameFramework/GameModeBase.h"
#include "MJBProdGameMode.generated.h"

UCLASS(minimalapi)
class AMJBProdGameMode : public AMJBProdGameModeBase
{
	GENERATED_BODY()

public:
	AMJBProdGameMode();
protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FireguardCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FireguardCountGoal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool FireGolemCanBeAttacked;
};



