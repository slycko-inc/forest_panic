// MJB UQAC/NAD


#include "MJBProd/GameModes/AreaTwoGameMode.h"

#include "Kismet/GameplayStatics.h"
#include "MJBProd/Helpers/UtilityHelpers.h"

AAreaTwoGameMode::AAreaTwoGameMode(): AMJBProdGameModeBase()
{
	//InGame attributes
	MaxTreeInFireCount = 10;
}

void AAreaTwoGameMode::GetTask(int TaskNumber, ACigarette*& CigaretteRef, AAshtray*& AshtrayRef)
{
	for (const auto Cigarette : TCigarettes)
	{
		ACigarette* _cigarette = Cast<ACigarette>(Cigarette);
		if(_cigarette != nullptr)
		{
			if(_cigarette->TaskNumber == TaskNumber)
			{
				CigaretteRef = _cigarette;
			}
		}
	}

	for (const auto Ashtray : TAshtrays)
	{			
		AAshtray* _ashtray = Cast<AAshtray>(Ashtray);
		if(_ashtray != nullptr)
		{
			if(_ashtray->TaskNumber == TaskNumber)
			{
				AshtrayRef = _ashtray;
			}
		}
	}
}

void AAreaTwoGameMode::ActivateTask(ACigarette* Cigarette, AAshtray* Ashtray)
{
	Cigarette->Activate();
	Cigarette->SetActorHiddenInGame(false);
	Cigarette->SetActorEnableCollision(true);
	Ashtray->SetActorEnableCollision(true);
}

void AAreaTwoGameMode::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACigarette::StaticClass(), TCigarettes);
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAshtray::StaticClass(), TAshtrays);
	
	OnBeginTask.AddDynamic(this, &AAreaTwoGameMode::BeginTask);
	OnEndTask.AddDynamic(this, &AAreaTwoGameMode::EndTask);
}

void AAreaTwoGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AAreaTwoGameMode::BeginTask()
{
	Super::BeginTask();

	// Task Handler
	switch (CurrentTaskNumber)
	{
		ACigarette* ACigarette;
		AAshtray* AAshtray;
	case 1:
		GetTask(CurrentTaskNumber, ACigarette, AAshtray);
		ActivateTask(ACigarette, AAshtray);
		UtilityHelpers::Log("1", true);
		break;
	case 2:
		GetTask(CurrentTaskNumber, ACigarette, AAshtray);
		ActivateTask(ACigarette, AAshtray);
		UtilityHelpers::Log("2", true);
		break;
	case 3:
		GetTask(CurrentTaskNumber, ACigarette, AAshtray);
		ActivateTask(ACigarette, AAshtray);
		UtilityHelpers::Log("3", true);
		break;
	default: ZoneTwoDone.Broadcast();break;
	}
}

void AAreaTwoGameMode::EndTask()
{
	Super::EndTask();
}
