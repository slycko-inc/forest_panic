﻿// MJB UQAC/NAD


#include "FireRiskBase.h"

#include "Kismet/KismetSystemLibrary.h"
#include "MJBProd/Helpers/UtilityHelpers.h"
#include "MJBProd/SceneComponents/SC_Flammable.h"


// Sets default values
AFireRiskBase::AFireRiskBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	IsSafe = false;
	BurnForestRadius = 100;
	TimeBeforePropagate = 75.0f;
	TimeBeforeFirstPropagation = 120.0f;
}

void AFireRiskBase::EnableFirePropagation() const
{
	// On entre en collision avec PhysicsBody et Flammable(GameTraceChannel2).
	// Voir DefaultGame.ini pour definition.
	auto ObjectTypes = TArray<TEnumAsByte<EObjectTypeQuery>>({
		UEngineTypes::ConvertToObjectType(ECC_PhysicsBody),
		UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel2)
	});

	TArray<FHitResult> Hits = TArray<FHitResult>();
	const TArray<AActor*> IgnoreList = TArray<AActor*>();

	const FVector SelfLocation = GetActorLocation();

	UKismetSystemLibrary::SphereTraceMultiForObjects(
		GetWorld(),
		SelfLocation,
		SelfLocation,
		BurnForestRadius,
		ObjectTypes,
		true,
		IgnoreList,
		EDrawDebugTrace::None,
		Hits,
		true
	);

	int NbObjectSetOnFire = 0;
	for (FHitResult hit : Hits)
	{
			USC_Flammable* SC_FlammableComponent = hit.GetActor()->FindComponentByClass<USC_Flammable>();
			
			if (SC_FlammableComponent != nullptr)
			{
				SC_FlammableComponent->SetIsOnFire(true);
				break;
			}
	}
}

// Called when the game starts or when spawned
void AFireRiskBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFireRiskBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFireRiskBase::Activate()
{
	IsDangerous = true;
	OnDangerUpdate.Broadcast();
	GetWorld()->GetTimerManager().SetTimer(
	FirePropagationTimer,
	this,
	&AFireRiskBase::EnableFirePropagation,
	TimeBeforePropagate,
	true,
	TimeBeforeFirstPropagation
	);
}

void AFireRiskBase::Deactivate()
{
	IsDangerous = false;
	GetWorld()->GetTimerManager().ClearTimer(FirePropagationTimer);
}

void AFireRiskBase::UnpauseTimerFirePropagation()
{
	GetWorld()->GetTimerManager().UnPauseTimer(FirePropagationTimer);
}

void AFireRiskBase::PauseTimerFirePropagation()
{
	GetWorld()->GetTimerManager().PauseTimer(FirePropagationTimer);
}


