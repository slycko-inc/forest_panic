// MJB UQAC/NAD


#include "MJBProd/GameModes/MJBProdGameModeBase.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "MJBProd/Helpers/UtilityHelpers.h"
#include "Sound/SoundCue.h"

class USoundCue;

AMJBProdGameModeBase::AMJBProdGameModeBase()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(
		TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	PrimaryActorTick.bCanEverTick = true;

	
	// Init du son de completion de tâche
	if(!TaskCompletedAudioCue)
	{
		static ConstructorHelpers::FObjectFinder<USoundCue> taskCompletedCue(TEXT("'/Game/Sound/Music/Step_Complete/StepCompletedSoundCue.StepCompletedSoundCue'"));
		TaskCompletedAudioCue = taskCompletedCue.Object;
	}
}

void AMJBProdGameModeBase::BeginTask()
{
	IsCurrentTaskCompleted = false;
}

void AMJBProdGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	
	ZeroTree = true;
}

void AMJBProdGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AMJBProdGameModeBase::EndTask()
{
	if(!ZeroTree && IsCurrentTaskCompleted)
	{
		OnTaskCompletedWithTreeInFire.Broadcast();
	}
	if(ZeroTree && IsCurrentTaskCompleted)
	{
		
		if(TaskCompletedAudioCue && CurrentTaskNumber < 3)
		{
			UGameplayStatics::PlaySound2D(this, TaskCompletedAudioCue);
		}
		CurrentTaskNumber += 1;
		OnStartEndTaskDialog.Broadcast();
		OnBeginTask.Broadcast();
	}
}

void AMJBProdGameModeBase::AddTreeFireCount(int NbToAdd)
{
	TreeInFireCount += NbToAdd;
	OnTreeFireCountUpdate.Broadcast();
	UtilityHelpers::Log(FString::FromInt(TreeInFireCount), true);
	UtilityHelpers::LogBool(TreeInFireCount == MaxTreeInFireCount, true);
	if (TreeInFireCount >= MaxTreeInFireCount)
	{
		GameOver = true;
		GameOverFire.Broadcast();
	}
	
	if(TreeInFireCount > 0)
	{
		ZeroTree = false;
	}
}

void AMJBProdGameModeBase::RemoveTreeFireCount(int NbToRemove)
{
	if (TreeInFireCount > 0)
	{
		TreeInFireCount -= NbToRemove;
	}
	
	if(TreeInFireCount == 0)
	{
		if(IsCurrentTaskCompleted)
			OnTaskCompletedWithOutTreeInFire.Broadcast();
		ZeroTree = true;
		OnEndTask.Broadcast();
	}
	
	OnTreeFireCountUpdate.Broadcast();
}
