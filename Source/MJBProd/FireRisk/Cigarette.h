// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "FireRiskBase.h"
#include "Cigarette.generated.h"

/**
 * 
 */
UCLASS()
class MJBPROD_API ACigarette : public AFireRiskBase
{
	GENERATED_BODY()

	public:	
	// Sets default values for this actor's properties
	ACigarette();

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
