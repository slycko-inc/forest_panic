// MJB UQAC/NAD


#include "FireProjectile.h"

#include "ChaosInterfaceWrapperCore.h"
#include "NiagaraEffectType.h"
#include "Kismet/GameplayStatics.h"
#include "MJBProd/MJBProdCharacter.h"
#include "MJBProd/SceneComponents/SC_Flammable.h"

void AFireProjectile::BeginPlay()
{
	Super::BeginPlay();
	CurrentGameMode = static_cast<AMJBProdGameModeBase*>(GetWorld()->GetAuthGameMode());
}

AFireProjectile::AFireProjectile()
{
	PrimaryActorTick.bCanEverTick = true;

	this->Damage = 20.0f;

	if(!RootComponent)
	{
		RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("FireProjectileSceneComponent"));
	}
	if(!CollisionComponent)
	{
		CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("FireProjectileSphereComponent"));
		CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("FireProjectile"));
		CollisionComponent->OnComponentHit.AddDynamic(this, &AFireProjectile::OnHit);
		CollisionComponent->InitSphereRadius(25.0f);
		RootComponent = CollisionComponent;
	}

	if(!ProjectileMovementComponent)
	{
		ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("FireProjectileMovementComponent"));
		ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
		ProjectileMovementComponent->InitialSpeed = 3000.0f;
		ProjectileMovementComponent->MaxSpeed = 3000.0f;
		ProjectileMovementComponent->bRotationFollowsVelocity = true;
		ProjectileMovementComponent->bShouldBounce = true;
		ProjectileMovementComponent->Bounciness = 0.3f;
		ProjectileMovementComponent->ProjectileGravityScale = 0.5f;
	}

	if(!ProjectileMeshComponent)
	{
		ProjectileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FireProjectileMeshComponent"));
		static ConstructorHelpers::FObjectFinder<UStaticMesh>Mesh(TEXT("'/Game/Meshes/SM_FireProjectile1.SM_FireProjectile1'"));
		if(Mesh.Succeeded())
		{
			ProjectileMeshComponent->SetStaticMesh(Mesh.Object);
		}
	}
	
	ProjectileMeshComponent->SetRelativeScale3D(FVector(2.0f, 2.0f, 2.0f));
	ProjectileMeshComponent->SetupAttachment(RootComponent);
}

void AFireProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFireProjectile::FireInDirection(const FVector& ShootDirection)
{
	Super::FireInDirection(ShootDirection);
}

void AFireProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit)
{
	Super::OnHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);
	
	APlayerController* Player = Hit.GetActor()->GetInstigatorController<APlayerController>();

	// Destroy bullet if it touch player
	if(Player != nullptr)
	{
		UGameplayStatics::ApplyDamage(Hit.GetActor(),20,nullptr, nullptr, nullptr);
		Destroy();
		return;
	}

	USC_Flammable *Flammable = Hit.Actor->FindComponentByClass<USC_Flammable>();
	if(Flammable != nullptr)
	{
		Flammable->SetIsOnFire(true);
		Destroy();
		return;
	}
	
	Destroy();
}
