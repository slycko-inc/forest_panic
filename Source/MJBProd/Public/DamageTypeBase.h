// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "DamageTypeBase.generated.h"

/**
 * 
 */
UCLASS()
class MJBPROD_API UDamageTypeBase : public UDamageType
{
	GENERATED_BODY()
};
