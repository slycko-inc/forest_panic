// MJB UQAC/NAD


#include "MJBProd/FireRisk/Fireguard.h"

// Sets default values
AFireguard::AFireguard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFireguard::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFireguard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

