// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MJBProd/GameModes/MJBProdGameModeBase.h"
#include "MJBProd/GameModes/TutorialGameMode.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "MJBProd/Projectile/ProjectileBase.h"
#include "Sound/SoundCue.h"
#include "Water_Projectile.generated.h"

UCLASS()
class MJBPROD_API AWater_Projectile : public AProjectileBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWater_Projectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void FireInDirection(const FVector& ShootDirection) override;
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit) override;
	void CallDestroy();

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
	USoundCue* ImpactAudioCue;

	UPROPERTY(BlueprintReadWrite, Category = "Physics")
	float ImpactForce = 10000;

};
