// MJB UQAC/NAD


#include "MJBProd/GameModes/AreaOneGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "MJBProd/Helpers/UtilityHelpers.h"

AAreaOneGameMode::AAreaOneGameMode(): AMJBProdGameModeBase()
{
	//InGame attributes
	MaxTreeInFireCount = 5;
}

void AAreaOneGameMode::GetTask(int TaskNumber, ACampfire*& CampfireRef, AFireguard*& FireguardRef)
{
	for (const auto Campfire : TCampfires)
	{
		ACampfire* Fire = Cast<ACampfire>(Campfire);
		if(Fire != nullptr)
		{
			if(Fire->TaskNumber == TaskNumber)
			{
				CampfireRef = Fire;
			}
		}
	}

	for (const auto Fireguard : TFireGuards)
	{			
		AFireguard* Guard = Cast<AFireguard>(Fireguard);
		if(Guard != nullptr)
		{
			if(Guard->TaskNumber == TaskNumber)
			{
				FireguardRef = Guard;
			}
		}
	}
}

void AAreaOneGameMode::ActivateTask(ACampfire* Campfire, AFireguard* Fireguard)
{
	Campfire->Activate();
	Fireguard->SetActorHiddenInGame(false);
	Fireguard->SetActorEnableCollision(true);
}

void AAreaOneGameMode::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACampfire::StaticClass(), TCampfires);
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFireguard::StaticClass(), TFireGuards);
	
	OnBeginTask.AddDynamic(this, &AAreaOneGameMode::BeginTask);
	OnEndTask.AddDynamic(this, &AAreaOneGameMode::EndTask);
}

void AAreaOneGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AAreaOneGameMode::BeginTask()
{
	Super::BeginTask();

	// Task Handler
	switch (CurrentTaskNumber)
	{
		AFireguard* Fireguard;
		ACampfire* Campfire;
	case 1:
		GetTask(CurrentTaskNumber, Campfire, Fireguard);
		ActivateTask(Campfire, Fireguard);
		UtilityHelpers::Log("1", true);
		break;
	case 2:
		GetTask(CurrentTaskNumber, Campfire, Fireguard);
		ActivateTask(Campfire, Fireguard);
		UtilityHelpers::Log("2", true);
		break;
	case 3:
		GetTask(CurrentTaskNumber, Campfire, Fireguard);
		ActivateTask(Campfire, Fireguard);
		UtilityHelpers::Log("3", true);
		break;
	default:
		ZoneOneDone.Broadcast();
		break;
	}
}

void AAreaOneGameMode::EndTask()
{
	Super::EndTask();
}
