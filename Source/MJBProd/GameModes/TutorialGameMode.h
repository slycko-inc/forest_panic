// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "MJBProdGameModeBase.h"
#include "GameFramework/GameModeBase.h"
#include "TutorialGameMode.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPutFireguardOnFire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnableNiagaraParticleOnFire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FActivateFireInTuto);
UCLASS()
class MJBPROD_API ATutorialGameMode : public AMJBProdGameModeBase
{
	GENERATED_BODY()

protected:
	
public:
	ATutorialGameMode();
	virtual void Tick(float DeltaSeconds) override;
protected:
	virtual void BeginPlay() override;
public:
	// Attributes

	// Events
	UPROPERTY(BlueprintAssignable,BlueprintCallable, Category = "Events")
	FActivateFireInTuto OnActivateFireInTutorial;
};
