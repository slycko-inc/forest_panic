﻿// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interact.generated.h"

// This class does not need to be modified.
UINTERFACE(Blueprintable, BlueprintType)
class UInteract : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MJBPROD_API IInteract
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Interact();
};
