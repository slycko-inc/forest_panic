// Copyright Epic Games, Inc. All Rights Reserved.

#include "MJBProdGameMode.h"

#include <string>

#include "MJBProd/MJBProdCharacter.h"
#include "MJBProd/Helpers/UtilityHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "UObject/ConstructorHelpers.h"

AMJBProdGameMode::AMJBProdGameMode():AMJBProdGameModeBase()
{
	//InGame attributes
	FireguardCountGoal = 3;
	FireGolemCanBeAttacked = false;
}

void AMJBProdGameMode::BeginPlay()
{
}

void AMJBProdGameMode::Tick(float DeltaSeconds)
{
	if(FireguardCount >= FireguardCountGoal)
	{
		FireGolemCanBeAttacked = true;
	}
}
