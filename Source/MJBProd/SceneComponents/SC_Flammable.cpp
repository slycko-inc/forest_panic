﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_Flammable.h"

#include "GeneratedCodeHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetArrayLibrary.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MJBProd/Helpers/UtilityHelpers.h"


// Sets default values for this component's properties
USC_Flammable::USC_Flammable()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	PropagationDelay = 8.0f;
	PropagationRadius = 525.0f;
	PropagationMaxByCycle = 1;
}


// Called when the game starts
void USC_Flammable::BeginPlay()
{
	Super::BeginPlay();
	// UGameplayStatics::GetAllActorsOfClass(GetWorld(), USC_Flammable::StaticClass(), TTree);
	this->OnFireStarted.AddDynamic(this, &USC_Flammable::StartFirePropagation);
	this->OnFireStopped.AddDynamic(this, &USC_Flammable::StopFirePropagation);

	GameMode = static_cast<AMJBProdGameModeBase*>(GetWorld()->GetAuthGameMode());
	if(bIsOnFire)
		OnFireStarted.Broadcast();
}

void USC_Flammable::StartFirePropagation()
{
	UtilityHelpers::Log(TEXT("StartFirePropagation"),false, this);

	GetWorld()->GetTimerManager().SetTimer(
		this->PropagationTimerHandle,
		this,
		&USC_Flammable::PropagateFire,
		this->PropagationDelay,
		true,
		this->PropagationDelay
		);
}

void USC_Flammable::PropagateFire()
{
	TArray<FHitResult> hits = TArray<FHitResult>();
	TArray<AActor*> ignoreList = TArray<AActor*>();
	FVector start = this->GetComponentTransform().GetLocation();
	FVector end = this->GetComponentTransform().GetLocation();

	// On entre en collision qu'avec les TraceChannel Flammable(GameTraceChannel2).
	// Voir DefaultGame.ini pour definition.
	ETraceTypeQuery TraceType = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel2);

	ignoreList.Add(this->GetOwner());

	UKismetSystemLibrary::SphereTraceMulti(
		GetWorld(),
		start,
		end,
		this->PropagationRadius,
		TraceType,
		true,
		ignoreList,
		EDrawDebugTrace::ForDuration,
		hits,
		true);

	if(this->PropagationMaxByCycle == 0)
		return;

	int NbObjectPropagated = 0;
	FCustomThunkTemplates::Array_Shuffle(hits);	
	for (FHitResult hit : hits)
	{
		UtilityHelpers::Log("HIT: " + hit.GetActor()->GetHumanReadableName(), false, this);
		
		USC_Flammable* SC_FlammableComponent = hit.GetActor()->FindComponentByClass<USC_Flammable>();

		if(SC_FlammableComponent != nullptr)
		{
			SC_FlammableComponent->SetIsOnFire(true);
			NbObjectPropagated++;

			if(this->PropagationMaxByCycle != -1 && NbObjectPropagated == this->PropagationMaxByCycle)
				break;
		}
	}
}

void USC_Flammable::StopFirePropagation()
{
	UtilityHelpers::Log(TEXT("StopFirePropagation"), false, this);

	GetWorld()->GetTimerManager().ClearTimer(this->PropagationTimerHandle);
}

// Called every frame
void USC_Flammable::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USC_Flammable::SetIsOnFire(bool isOnFire)
{
	
	if(this->bIsOnFire != isOnFire)
	{
		if(isOnFire && bIsBurned)
			return;
		
		this->bIsOnFire = isOnFire;

		if(isOnFire)
			OnFireStarted.Broadcast();
		else
			OnFireStopped.Broadcast();
	}
}

void USC_Flammable::SetIsBurned(bool isBurned)
{
	if(this->bIsBurned != isBurned)
	{
		UtilityHelpers::Log(TEXT("SetIsBurned: " + UKismetStringLibrary::Conv_BoolToString(isBurned)), false, this);
		this->SetIsOnFire(false);
		this->bIsBurned = isBurned;
	}
}

