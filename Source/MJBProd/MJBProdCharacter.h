// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MJBProd/Projectile/Water_Projectile.h"
#include "MJBProdCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCanInteract);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCannotInteract);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeath);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeathAnimStart);

UENUM(BlueprintType)
enum class EPickable : uint8 {
	E_FireGuard       UMETA(DisplayName="FireGuard"),
	E_Cigarette       UMETA(DisplayName="Cigarette"),
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDropAnim, EPickable, PickableType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPickupAnim, EPickable, PickableType);

UCLASS(config=Game)
class AMJBProdCharacter : public ACharacter
{
	GENERATED_BODY()
	FTimerHandle StunTimerHandle;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UFUNCTION()
	void Dead();

public:
	AMJBProdCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Mesh)
	class USkeletalMeshComponent* Gun_Mesh;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USceneComponent* ProjectileSpawnerLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	float CameraOffset;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AWater_Projectile> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool FireProjectile;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= Weapon)
	bool isZoomIn;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FCanInteract OnCanInteract;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FCannotInteract OnCannotInteract;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FDeath OnDeath;
	
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FDeathAnimStart OnDeathAnimStart;
	
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FPickupAnim OnPickupAnim;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FDropAnim OnDropAnim;

	UPROPERTY(BlueprintReadOnly)
	bool IsDead;

	UPROPERTY()
	AMJBProdGameModeBase* CurrentGameMode;

	UFUNCTION(BlueprintCallable)
	void ZoomOut();

protected:
	UFUNCTION()
	void Fire();

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	* Called via input to turn at a given rate. 
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate. 
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void DisableStun();
	void CheckInteract();

	void Interact();

	void ToggleDebug();

	void ZoomIn();

	bool HasFireGuard;
	bool HasCigarette;

	UPROPERTY()
	UStaticMeshComponent* FireGuardMesh;
	
	UPROPERTY()
	UStaticMeshComponent* CigaretteMesh;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintCallable)
	void ReceiveKnockback(FVector Force, int Damage = 0);
	
	virtual void BeginPlay();
};
