// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AICharacterBase.generated.h"

UCLASS()
class MJBPROD_API AAICharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAICharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UFUNCTION(BlueprintCallable)
 	static void UpdateWalkingSpeed(AAICharacterBase *Character, float Speed);
 
 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
 	float ChaseSpeed = 500.0f;
 	
 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float WalkSpeed = 300.0f;
 
 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
 	float Radius = 2000.0f;

	// Line of sight duration timer
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LineOfSightTimerLengh = 3.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstance *VulnerableMaterial;
};
