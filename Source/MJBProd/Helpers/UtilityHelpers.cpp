// Fill out your copyright notice in the Description page of Project Settings.


#include "UtilityHelpers.h"

#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

UtilityHelpers::UtilityHelpers()
{
}

UtilityHelpers::~UtilityHelpers()
{
}

void UtilityHelpers::PrintToScreen(FString Msg, bool AtScreen)
{
	// Print in the screen if GEngine exists
	if(GEngine && AtScreen)
	{
		GEngine->AddOnScreenDebugMessage(-1,5.0f,FColor::Green,Msg);
	}
}

void UtilityHelpers::Log(FString Msg, bool AtScreen, UObject * object)
{
	if(object != nullptr)
	 Msg.InsertAt(0,UKismetSystemLibrary::GetDisplayName(object) + ": " );	
	
	UtilityHelpers::PrintToScreen(Msg, AtScreen);
	// Print to Unreal log
	UE_LOG(LogTemp, Display, TEXT("%s"), *Msg);
}

void UtilityHelpers::LogFloat(float Msg, bool AtScreen, UObject* object)
{
	Log(UKismetStringLibrary::Conv_FloatToString(Msg), AtScreen, object);
}

void UtilityHelpers::LogBool(bool Msg, bool AtScreen, UObject* object)
{
	Log(UKismetStringLibrary::Conv_BoolToString(Msg), AtScreen, object);
}

void UtilityHelpers::LogWarning(FString Msg, bool AtScreen, UObject * object)
{
	if(object != nullptr)
	 Msg.InsertAt(0,UKismetSystemLibrary::GetDisplayName(object) + ": " );
	
	UtilityHelpers::PrintToScreen(Msg, AtScreen);
	// Print to Unreal log
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Msg);
}

void UtilityHelpers::LogError(FString Msg, bool AtScreen, UObject * object)
{
	if(object != nullptr)
	 Msg.InsertAt(0,UKismetSystemLibrary::GetDisplayName(object) + ": " );
	
	UtilityHelpers::PrintToScreen(Msg, AtScreen);
	// Print to Unreal log
	UE_LOG(LogTemp, Error, TEXT("%s"), *Msg);
}
