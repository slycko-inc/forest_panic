﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "MJBProd/GameModes/MJBProdGameMode.h"

#include "SC_Flammable.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FFireStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FFireStopped);

UCLASS(ClassGroup=(Custom), meta=(IsBlueprintBase="true"))
class MJBPROD_API USC_Flammable : public USceneComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	USC_Flammable();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere, BlueprintGetter=GetIsOnFire, BlueprintSetter=SetIsOnFire)
	bool bIsOnFire;
	
	UPROPERTY(EditAnywhere, BlueprintGetter=GetIsBurned, BlueprintSetter=SetIsBurned)
	bool bIsBurned;
		
	UFUNCTION()
	void StartFirePropagation();
	UFUNCTION()
	void PropagateFire();
	UFUNCTION()
	void StopFirePropagation();

	FTimerHandle PropagationTimerHandle;
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PropagationDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PropagationRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,
		DisplayName="Nombre d'objets incendiés par cycle de propagation")
	int PropagationMaxByCycle;

	UPROPERTY()
	AMJBProdGameModeBase* GameMode;

	UPROPERTY()
	TArray<AActor*> TTree;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FFireStarted OnFireStarted;
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FFireStopped OnFireStopped;
	
	UFUNCTION(BlueprintCallable)
	void SetIsOnFire(bool isOnFire);
	
	UFUNCTION(BlueprintPure)
	bool GetIsOnFire() {return this->bIsOnFire; }
	
	UFUNCTION(BlueprintCallable)
	void SetIsBurned(bool isBurned);
	
	UFUNCTION(BlueprintPure)
	bool GetIsBurned() {return this->bIsBurned; }
};
