// MJB UQAC/NAD


#include "Campfire.h"

#include "NiagaraComponent.h"
#include "MJBProd/Helpers/UtilityHelpers.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
ACampfire::ACampfire()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	// InGame attributes
	BurnForestRadius = 500;
	TimeBeforePropagate = 5.0f;
	TimeBeforeFirstPropagation = 12.0f;
}

// Called when the game starts or when spawned
void ACampfire::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACampfire::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACampfire::Deactivate()
{
	Super::Deactivate();
	
	OnDangerUpdate.Broadcast();
	
	TSet<UActorComponent*> Components = GetComponents();
	for (UActorComponent* Component : Components)
	{
		if(Component->GetName() == "PareFeu")
		{
			UStaticMeshComponent *Mesh = Cast<UStaticMeshComponent>(Component);
			Mesh->SetVisibility(true);
		}
	}
}

