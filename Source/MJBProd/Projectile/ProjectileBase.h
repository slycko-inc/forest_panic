﻿// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "MJBProd/GameModes/MJBProdGameMode.h"
#include "MJBProd/GameModes/TutorialGameMode.h"
#include "ProjectileBase.generated.h"

UCLASS()
class MJBPROD_API AProjectileBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProjectileBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// GameMode
	UPROPERTY()
	AMJBProdGameModeBase* CurrentGameMode;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Base attributes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
	USphereComponent* CollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
	UStaticMeshComponent* ProjectileMeshComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	UMaterial* ProjectileMaterialInstance;

	// Base function
	virtual void FireInDirection(const FVector& ShootDirection);

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
};
