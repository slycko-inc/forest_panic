// MJB UQAC/NAD

#include "Cigarette.h"

// Sets default values
ACigarette::ACigarette()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACigarette::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACigarette::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}