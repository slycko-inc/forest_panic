// MJB UQAC/NAD

#pragma once

#include "CoreMinimal.h"

#include "NiagaraComponent.h"
#include "MJBProd/FireRisk/FireRiskBase.h"
#include "GameFramework/Actor.h"
#include "Campfire.generated.h"

UCLASS()
class MJBPROD_API ACampfire : public AFireRiskBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACampfire();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual void Deactivate() override;

	// Events
};
